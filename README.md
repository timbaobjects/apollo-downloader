# apollo-downloader #

apollo-downloader is a helper script to automate downloading of submission data from an Apollo instance

	usage: downloader.py [-h] -e EMAIL -p PASSWORD -o OUTFILE url

	A download script for Apollo's exports

	positional arguments:
	  url                   The url to initiate the download from

	optional arguments:
	  -h, --help            show this help message and exit
	  -e EMAIL, --email EMAIL
                        The email address for the Apollo account
	  -p PASSWORD, --password PASSWORD
                        The password for the Apollo account
	  -o OUTFILE, --outfile OUTFILE
                        The file to save the output to