#!/usr/bin/env python
from urlparse import urlparse
import argparse
import mechanize
import logging
import sys

DEBUG = False

parser = argparse.ArgumentParser(description="A download script for Apollo's exports")
parser.add_argument('-e', '--email', required=True, help='The email address for the Apollo account')
parser.add_argument('-p', '--password', required=True, help='The password for the Apollo account')
parser.add_argument('-o', '--outfile', required=True, help="The file to save the output to")
parser.add_argument('url', help='The url to initiate the download from')

args = parser.parse_args()

url = urlparse(args.url)
base = '{scheme}://{netloc}'.format(scheme=url.scheme, netloc=url.netloc)
auth = '{base}/accounts/login'.format(base=base)

br = mechanize.Browser()

# debugging
if DEBUG:
    br.set_debug_redirects(True)
    br.set_debug_responses(True)
    br.set_debug_http(True)

logger = logging.getLogger("mechanize")
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)

br.open(auth)
br.select_form(name="login_user_form")
br['email'] = args.email
br['password'] = args.password
r = br.submit()

br.retrieve(args.url, args.outfile)
